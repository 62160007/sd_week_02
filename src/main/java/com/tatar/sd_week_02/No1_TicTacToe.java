/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tatar.sd_week_02;

import java.util.*;

/**
 *
 * @author Dell
 * 62160007 Thanawat Thongtitcharoen Sec 2
 */
public class No1_TicTacToe {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        String row = "", col = "";
        int counter = 0;
        showWelcome();
        for (int i = 0; i < 9; i++) {
            showTable(table);
            char turn = showTurn(counter);
            boolean check = true;
            do {
                if (checkInput(row = kb.next(), col = kb.next(), table) == 0) {
                    check = false;
                }
            } while (check);
            inputTable(row, col, table, turn);
            if (checkWin(table) != 0) {
                showResult(table, turn, 1);
                break;
            }
            if (counter == 8) {
                showResult(table, turn, 2);
            }
            counter++;
        }
    }

    public static char showTurn(int counter) {
        char turn = findTurn(counter);
        System.out.println(turn + " turn");
        System.out.println("Please input Row Col: ");
        return turn;
    }

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void showTable(char table[][]) {
        System.out.println("  1 2 3");
        for (int i = 0; i < 3; i++) {
            System.out.print(i + 1 + " ");
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public static char findTurn(int turn) {
        if (turn % 2 == 0) {
            return 'X';
        }
        return 'O';
    }

    public static void inputTable(String row, String col,
            char table[][], char turn) {
        int rowi = converseInt(row);
        int coli = converseInt(col);
        table[rowi - 1][coli - 1] = turn;
    }

    public static int converseInt(String sti) {
        return Integer.parseInt(sti);
    }

    public static int checkInput(String row, String col, char table[][]) {
        int code = 0;
        if (row.length() == 1 && col.length() == 1) {
            if ((row.equals("1") || row.equals("2") || row.equals("3"))
                    && (col.equals("1") || col.equals("2")
                    || col.equals("3"))) {
                if (table[converseInt(row) - 1][converseInt(col) - 1] != '-') {
                    code = 4;
                }
            } else {
                code = 1;
            }
        } else {
            code = 2;
        }
        return printerrCode(code);
    }

    public static int printerrCode(int code) {
        switch (code) {
            case 1:
                System.out.println("Input between 1-3 , Please input again");
                break;
            case 2:
                System.out.println("Input number only , Please input again");
                break;
            case 4:
                System.out.println("Can't Input this location , "
                        + "Please input again");
                break;
        }
        return code;
    }

    public static int checkWin(char table[][]) {
        int result = 0;
        result += checkCol(table);
        result += checkRow(table);
        result += checkX1(table);
        result += checkX2(table);
        if (result > 0) {
            return 1;
        }
        return 0;
    }

    public static void showResult(char table[][], char turn, int type) {
        showTable(table);
        if (type == 1) {
            System.out.println("Player " + turn + " Win..." + "\nBye bye ....");
        } else if (type == 2) {
            System.out.println("No player Win..." + "\nBye bye ....");
        }
    }

    public static int checkRow(char table[][]) {
        for (int i = 0; i < 3; i++) {
            if (table[i][0] == 'X' && table[i][1] == 'X'
                    && table[i][2] == 'X') {
                return 1;
            } else if (table[i][0] == 'O' && table[i][1] == 'O'
                    && table[i][2] == 'O') {
                return 1;
            }
        }
        return 0;
    }

    public static int checkCol(char table[][]) {
        for (int i = 0; i < 3; i++) {
            if (table[0][i] == 'X' && table[1][i] == 'X'
                    && table[2][i] == 'X') {
                return 1;
            } else if (table[0][i] == 'O' && table[1][i] == 'O'
                    && table[2][i] == 'O') {
                return 1;
            }
        }
        return 0;
    }

    public static int checkX1(char table[][]) {
        if (table[0][0] == 'X' && table[1][1] == 'X' && table[2][2] == 'X') {
            return 1;
        } else if (table[0][0] == 'O' && table[1][1] == 'O'
                && table[2][2] == 'O') {
            return 1;
        }
        return 0;
    }

    public static int checkX2(char table[][]) {
        if (table[2][0] == 'X' && table[1][1] == 'X' && table[0][2] == 'X') {
            return 1;
        } else if (table[2][0] == 'O' && table[1][1] == 'O'
                && table[0][2] == 'O') {
            return 1;
        }
        return 0;
    }

}
